package edu.ucsd.cse110.client;

import java.net.URISyntaxException;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.util.IndentPrinter;

public class ChatClient {
	
	private MessageProducer producer;
	private Session session;
	
	public ChatClient(MessageProducer producer, Session session) {
		super();
		this.producer = producer;
		this.session = session;
	} 
	
	/**
	 * Sends a message to the server
	 * @param msg The message to be sent
	 * @throws JMSException
	 */
	public void send(String msg) throws JMSException {
		producer.send(session.createTextMessage(msg));
	}
	
	
	public static void main(String[] args) {
		try {
			// start connection
			ActiveMQConnection connection = ActiveMQConnection.makeConnection(/*user, password,*/ Constants.URL);
			connection.start();
			
			// start session
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			
			// create queue
			Queue destQueue = session.createQueue(Constants.PRO_QUEUE);
			// create client's producer
			MessageProducer producer = session.createProducer(destQueue);
			
			// init client with the session and producer
			ChatClient client = new ChatClient(producer, session);
			
			// send "Hi" to the server
			client.send("Hi");
			System.out.println("Client sent message");
			
			
			// Create client's consumer
			MessageConsumer consumer = session.createConsumer(session.createQueue(Constants.CON_QUEUE));
			Message message = consumer.receive(1000);
			
			// when consumer receives a message, display it
			if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                String text = textMessage.getText();
                System.out.println("Received: " + text);
            } else {
                System.out.println("Received: " + message);
            }
			
		} catch (JMSException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
}

