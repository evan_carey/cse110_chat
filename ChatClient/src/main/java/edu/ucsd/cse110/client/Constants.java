package edu.ucsd.cse110.client;
/**
 * Client constants
 * @author jtbishop
 *
 */
public class Constants {

	public static final String URL = "tcp://localhost:61616";
	
	/* The queue client uses to send messages to sever */
	public static final String PRO_QUEUE = "clientToServer";
	
	/* The queue for client to receive messages from server */
	public static final String CON_QUEUE = "serverToClient";
}

