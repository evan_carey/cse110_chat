package edu.ucsd.cse110.server;

/**
 * Server Constants
 * @author jtbishop
 *
 */
public class Constants {

	public static final String URL = "tcp://localhost:61616";
	
	/* The queue that the server uses to send messages to client */
	public static final String PRO_QUEUE = "serverToClient";
	
	/* The queue for server to receive messages from client */
	public static final String CON_QUEUE = "clientToServer";
}

