package edu.ucsd.cse110.server;

public class Server {
	
	/**
	 * Server receives a message from a client, sends it to the producer,
	 * which sends it to clients.
	 * @param msg The message being received
	 */
	public void receive(String msg) {
		System.out.println(msg + " received by server");

		new ServerProducer(msg + " to you too!").run();

		System.out.println("Producer object created");
	}
}

