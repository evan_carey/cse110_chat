package edu.ucsd.cse110.server;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;


public class ChatServer {
	

	public static void main(String[] args) throws Exception {
		// start broker
		BrokerService broker = new BrokerService();
		broker.addConnector(Constants.URL);
		broker.setPersistent(false);
		broker.start();
		System.out.println("Broker Started");
		
		// run server
		new ChatServer().run();
	}
	
	
	
	public void run() {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ChatServer.class);
		
		/*
		MessageCreator messageCreator = new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage("ping!");
			}
		};
		*/
		
		// creates the framework template bean things
		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
		
		/*
		jmsTemplate.setConnectionFactory(connectionFactory);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.send(QUEUE_NAME, messageCreator);
		*/
	}
	
	/**
	 * Sets the server message receiver to the Server class
	 * @return
	 */
	@Bean
	MessageListenerAdapter receiver() {
		return new MessageListenerAdapter(new Server()) {
			{
				setDefaultListenerMethod("receive");
			}
		};
	}

	/**
	 * Creates the JMS template for the connection factory
	 * @param connectionFactory
	 * @return
	 */
	@Bean
	JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
		return new JmsTemplate(connectionFactory);
	}
	
	/**
	 * Creates the connection factory at the URL
	 * @return
	 */
	@Bean
	ConnectionFactory connectionFactory() {
		return new CachingConnectionFactory(new ActiveMQConnectionFactory(Constants.URL));
	}
	
	/**
	 * Creates the server's listener.
	 * @param messageListener
	 * @param connectionFactory
	 * @return
	 */
	@Bean
	SimpleMessageListenerContainer container(final MessageListenerAdapter messageListener, final ConnectionFactory connectionFactory) {
		return new SimpleMessageListenerContainer() {
			{
				setMessageListener(messageListener);
				setConnectionFactory(connectionFactory);
				setDestinationName(Constants.CON_QUEUE);
			}
		};
	}
}

